import React, { useState } from 'react';
import Header from '../Header/Header';
import { Link } from 'react-router-dom';
import { FaListUl, FaPlus } from 'react-icons/fa';
import Item from '../Item/Item';
import ListBoard from '../ListBoard/ListBoard';

function Map(props) {
  const [itemRelative, setItemRelative] = useState('');
  const [itemId, setItemId] = useState('');

  function clickItem(params) {
    setItemId(params);
    setItemRelative('list-board__item_focus');
  }

  return (
    <div className='map'>
      <Header />
      <div className='todo-list'>
        <div className='admin-menu'>
          <Link to='/'>
            <FaListUl className='header__logo link_hover' />
          </Link>
          <div className='admin-menu__list'>
            {
              <ul style={{ padding: 0, margin: 0 }}>
                {props.state.map((item) => (
                  <Item
                    key={item.id}
                    {...item}
                    item={item}
                    clickItem={clickItem}
                    popupAddProject={props.popupAddProject}
                  />
                ))}
              </ul>
            }
          </div>
          <button
            className='admin-menu__button'
            onClick={() => {
              props.popupAddProject(true);
            }}>
            <FaPlus className='admin-menu__button-img' />
          </button>
          <label className='admin-menu__button-label'>Добавить проект</label>
        </div>
        <div className='screen-blocks'>
          <ListBoard
            listBoard={props.state}
            itemRelative={itemRelative}
            itemId={itemId}
            data={props.data}
            // arrData={props.arrData}
          />
        </div>
      </div>
    </div>
  );
}

export default Map;
