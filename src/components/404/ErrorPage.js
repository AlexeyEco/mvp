import React from 'react';
import { Link } from 'react-router-dom';
import { FaFrown } from 'react-icons/fa';

function ErrorPage() {
  return (
    <div className='errorPage'>
      <FaFrown className='errorPage__img' />
      <p className='errorPage__paragraph'>Страница не найдена</p>
      <Link to='/' className='errorPage__link link_hover'>
        Назад
      </Link>
    </div>
  );
}

export default ErrorPage;
