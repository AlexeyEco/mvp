import React, { useState } from 'react';
import { Route, Switch, Link, useHistory } from 'react-router-dom';
import { FaBars } from 'react-icons/fa';
import { FaUserCircle } from 'react-icons/fa';
import { AiOutlineClose } from 'react-icons/ai';

function Navigation(props) {
  const history = useHistory();
  const [sidebar, setSidebar] = useState(false);
  const [overlay, setOverlay] = useState(false);

  function showSidebar() {
    setSidebar(!sidebar);
    setOverlay(!overlay);
  }

  return (
    <Switch>
      {props.loggedIn === false ? (
        <Route exact path='/'>
          <ul className='nav nav_main'>
            <li className='nav__li nav__li_main link_hover'>
              <Link to='/signup' className='nav__link'>
                Регистрация
              </Link>
            </li>
            <li>
              <button
                onClick={() => {
                  history.push('/signin');
                }}
                className='nav__button link_hover'>
                Войти
              </button>
            </li>
          </ul>
        </Route>
      ) : (
        <Route>
          <Link to='#' className={`nav__bar`}>
            <FaBars
              className='nav__bar-icon link_hover'
              onClick={showSidebar}
            />
          </Link>
          <div className={overlay ? 'overlay active' : 'overlay'}></div>
          <nav className={sidebar ? 'nav__menu active' : 'nav__menu'}>
            <Link to='#' className='nav__bar ' onClick={showSidebar}>
              <AiOutlineClose className='nav__bar-icon nav__bar-icon_close link_hover' />
            </Link>
            <ul className='nav nav_menu'>
              <button
                onClick={() => {
                  history.push('/profile');
                }}
                className='nav__button
              nav__button_auth link_hover'>
                <FaUserCircle className='nav__button-img' />
              </button>
              <li className='nav__li nav__li_bar link_hover'>
                <Link to='/' className='nav__link nav__link_color'>
                  Главная
                </Link>
              </li>
              <li className='nav__li nav__li_bar-margin link_hover'>
                <Link to='/map' className='nav__link nav__link_color'>
                  Голосов осталось: 1
                </Link>
              </li>
            </ul>
          </nav>
        </Route>
      )}
    </Switch>
  );
}

export default Navigation;
