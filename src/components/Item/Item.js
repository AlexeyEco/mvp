import React, { useContext } from 'react';
import { Context } from '../../contexts/context';
import { FaTrashAlt, FaPlus } from 'react-icons/fa';

export default function Item({ title, id, completed, clickItem, popupAddProject }) {
  const { dispatch } = useContext(Context);

  const cls = ['todo'];

  if (completed) {
    cls.push('completed');
  }

  return (
    <li
      className={`${cls}`}
      onClick={() => {
        clickItem(id);
      }}>
      <button className={'todo__item'}>
        <span>{title}</span>
        <div className='todo__item-block-button'>
          <span
            className='todo__item-button'
            onClick={() => {
              // console.log('Добавить пользователя');
              popupAddProject(true);
            }}>
            <FaPlus className='admin-menu__button-img todo__item-button-img' />
          </span>
          <span
            className='todo__item-button'
            onClick={() =>
              dispatch({
                type: 'remove',
                payload: id,
              })
            }>
            <FaTrashAlt className='admin-menu__button-img todo__item-button-img' />
          </span>
        </div>
      </button>
    </li>
  );
}
