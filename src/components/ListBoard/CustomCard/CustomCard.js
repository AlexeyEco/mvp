import React, { useState } from 'react';

export default function CustomCard(
  props
  //   {
  //   // id,
  //   name,
  //   dueOn,
  //   subTitle,
  //   body,
  //   escalationText,
  //   cardColor,
  //   // cardStyle,
  //   // laneId,
  // }
) {
  // var a = ["a", "b", "c"];
  // const dataCard = props.data.lanes.map((i) => {
  //   // return i;
  //   let index;
  //   for (index = 0; index < i.cards.length; ++index) {
  //     return i.cards[index];
  //     // console.log(arrData[index]);
  //   }
  //   // console.log(index);
  //   // return index;
  // });
  // function countMethods(a){
  //   let b = {};
  //   a.forEach(p => {
  //     if( b.hasOwnProperty(p.Method) ){
  //       b[p.Method].Amount += p.Amount;
  //       b[p.Method].Length ++;
  //     }else{
  //       b[p.Method] = {"Method": p.Method, "Length":1, "Amount": p.Amount}
  //     }
  //   });
  //   return Object.values(b);
  // }

  // const dataCard = arrData.map((element) => {
  //   for (let index = 0; index < element.length; ++index) {
  //     return index;
  //     // console.log(arrData[index]);
  //   }
  //   // return element;
  //   // console.log(element);
  // });

  // let dataObj;
  // dataCard.map((res) => {
  //   // res.map((i) => console.log(i));
  //   console.log(res);
  //   return (dataObj = res);
  // });

  // const objCard = () => {
  //   const data = dataCard.forEach((res) => dataObj === res);
  //   // for (let object of dataCard) {
  //   //   return object;
  //   //   // for (let key of keys) {
  //   //   //   console.log(key);
  //   // }
  //   // for (let index = 0; index < dataCard.length; ++index) {
  //   //   return dataCard[index];
  //   //   // console.log(arrData[index]);
  //   // }
  //   return data;
  // };

  console.log(props.dataObj);
  // function getValues(array) {
  //   for (let object of array) {
  //     let keys = Object.entries(object);
  //     console.log(keys);
  //     // for (let key of keys) {
  //     //   console.log(key);
  //     // }
  //   }
  //   // let values = [];
  //   // console.log(search)
  //   //  return array.forEach(function (value) {
  //   //     // console.log(values);
  //   //    return values.push(value);
  //   // });

  //   // return values;
  // }
  // getValues(arrData);
  // console.log(props.data.);

  // arrData.forEach((element) => {
  //   return element;
  //   // console.log(element);
  // });
  //   arrData.forEach(element => {
  //     console.log(element)
  // });
  // function getKeys(obj) {
  //   return Object.keys(obj).map((key) => {
  //     return key;
  //     // return [key, getKeys(obj[key])];
  //   });
  // }
  // const Keys = getKeys(arrData);
  // const obj = Object.entries(arrData)
  // console.log(
  //   obj
  //   // arrData.map(
  //   //   (i) =>
  //   //     console.log(i[name])
  //   // )
  // );
  return (
    // <div className=''>
    //   {arrData.forEach(
    //     (
    //       data
    //       // {
    //       //   //   // id,
    //       //   name,
    //       //   dueOn,
    //       //   subTitle,
    //       //   body,
    //       //   escalationText,
    //       //   cardColor,
    //       //   // cardStyle,
    //       //   // laneId,
    //       // }
    //     ) => {
    // console.log(data.name);
    <div
      style={{
        backgroundColor: '#deceb0',
        padding: '10px',
        boxShadow: '4px 4px 4px 0px rgba(34, 60, 80, 0.2)',
        marginBottom: '10px',
      }}>
      <header
        style={{
          borderBottom: '1px solid #eee',
          paddingBottom: 6,
          marginBottom: 10,
          display: 'flex',
          flexDirection: 'row',
          justifyContent: 'space-between',
          color: props.dataObj.cardColor,
        }}>
        <div style={{ fontSize: 14, fontWeight: 'bold' }}>
          {props.dataObj.name}
        </div>
        <div style={{ fontSize: 11 }}>{props.dataObj.dueOn}</div>
      </header>
      <div style={{ fontSize: 12, color: '#BD3B36' }}>
        <div style={{ color: '#4C4C4C', fontWeight: 'bold' }}>
          {props.dataObj.subTitle}
        </div>
        <div style={{ padding: '5px 0px' }}>
          <i>{props.dataObj.body}</i>
        </div>
        <div
          style={{
            marginTop: 10,
            textAlign: 'center',
            color: props.dataObj.cardColor,
            fontSize: 15,
            fontWeight: 'bold',
          }}>
          {props.dataObj.escalationText}
        </div>
      </div>
    </div>
    //     }
    //   )}
    // </div>
  );
}
