import React from 'react';
// import Item from '../Item/Item';
// import data from '../../data.json';
// import Board, { createTranslate } from 'react-trello';
// import CustomCard from './CustomCard/CustomCard';
// import NewCardForm from './NewCardForm/NewCardForm';
// import ReactDOM from 'react-dom';
import AppBoard from '../ListBoard/Board/App';

export default function ListBoard(props) {
  // console.log(props.data)

  // const TEXTS = {
  //   'Add another lane': 'NEW LANE',
  //   'Click to add card': 'Добавить задачу',
  //   'Delete lane': 'Delete lane',
  //   'Lane actions': 'Lane actions',
  //   button: {
  //     'Add lane': 'Add lane',
  //     'Add card': 'Добавить',
  //     Cancel: 'Отменить',
  //   },
  //   placeholder: {
  //     title: 'Заголовок',
  //     description: 'Описание',
  //     label: 'Время выполнения',
  //   },
  // };

  // const arrData = props.data.lanes.map((i) => {
  //   let index;
  //   for (index = 0; index < i.cards.length; ++index) {
  //     return i.cards[index];
  //     // console.log(arrData[index]);
  //   }
  // });
  // console.log(arrData);

  // const dataCard = props.data.lanes.map((i) => {
  //   // return i;
  //   let index;
  //   for (index = 0; index < i.cards.length; ++index) {
  //     return i.cards[index];
  //     // console.log(arrData[index]);
  //   }
  //   // console.log(index);
  //   // return index;
  // });

  // let dataObj;
  // dataCard.map((res) => {
  //   // res.map((i) => console.log(i));
  //   console.log(res);
  //   return (dataObj = res);
  // });

  // const components = {
  //   AddCardLink: () => <button className='add-button' onClick={()=>{console.log('click')}}>Добавить задачу</button>,
  //   // LaneHeader: () => <div className="custom-header"></div>,
  //   NewCardForm: () => <NewCardForm />,
  //   // NewLaneSection: () => NewLane,
  //   Card: () => {
  //     return dataCard.map(
  //       (
  //         data
  //         // {
  //         //   //   // id,
  //         //   name,
  //         //   dueOn,
  //         //   subTitle,
  //         //   body,
  //         //   escalationText,
  //         //   cardColor,
  //         //   // cardStyle,
  //         //   // laneId,
  //         // }
  //       ) => {
  //         return (
  //           <CustomCard
  //             dataObj={data}
  //             // name={name}
  //             // dueOn={dueOn}
  //             // subTitle={subTitle}
  //             // body={body}
  //             // escalationText={escalationText}
  //             // cardColor={cardColor}
  //           />
  //         );
  //       }
  //     );
  //   },
  // };

  return (
    <ul className={'list-board'}>
      {props.listBoard.map((item, _id) => (
        <li
          key={_id}
          className={
            props.itemId === item.id
              ? `${props.itemRelative}`
              : 'list-board__item'
          }>
          <AppBoard
            // style={{
            //   backgroundColor: '#DCDCDC',
            //   width: '100%',
            //   height: '89vh',
            // }}
          />
          {/* <Board
            style={{ backgroundColor: '#DCDCDC', height: '89vh' }}
            key={_id}
            // {...item}
            data={props.data}
            draggable
            editable
            // addCardLink='ADD CARD'
            components={components}
            // t={createTranslate(TEXTS)}
            customCardLayout></Board> */}
        </li>
      ))}
    </ul>
  );
}
