import { React } from 'react';
import Popup from '../Popup/Popup';

function InfoTooltip(props) {
  return (
    <Popup
      isOpen={props.isOpen}
      onClose={props.onClose}
      infoTool={props.infoTool}
      img={props.infoTool.img}
      message={props.infoTool.message}
    />
  );
}

export default InfoTooltip;
