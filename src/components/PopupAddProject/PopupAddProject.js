import { React,  } from 'react';
import Popup from '../Popup/Popup';

function PopupAddProject(props) {
  return (
    <Popup
      isOpen={props.isOpen}
      onClose={props.onClose}
      infoTool={props.infoTool}
      img={props.infoTool.img}
      message={props.infoTool.message}
      todoTitle={props.todoTitle}
      addTodo={props.addTodo}
      addTodoButton={props.addTodoButton}
      state={props.state}
      setTodoTitle={props.setTodoTitle}
    />
  );
}

export default PopupAddProject;