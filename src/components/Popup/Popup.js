import React from 'react';
import { FaTimes, FaPlus } from 'react-icons/fa';
// import ListBoard from '../ListBoard/ListBoard';
// import 'materialize-css';

function Popup(props) {
  // console.log(props)
  return (
    <div className={props.isOpen ? `popup popup_open` : `popup`}>
      <div className='popup__overlay' onClick={props.onClose}></div>
      <div className={`popup__container popup__container_mobile`}>
        <button
          className={`popup__close-icon popup__close-icon_mobile`}
          onClick={props.onClose}>
          <FaTimes className='popup__close-icon-img' />
        </button>
        <div className='infoTool'>
          {/* <img src={props.img} alt='Картинка регистрации' />
          <p className='infoTool__title infoTool__title_mobile'>
            {props.message}
          </p> */}
          <div className='container'>
            <h1 style={{ fontSize: '22px' }}>Новый проект</h1>

            <div className='input-field'>
              <input
                type='text'
                value={props.todoTitle}
                onChange={(event) => props.setTodoTitle(event.target.value)}
                onKeyPress={props.addTodo}
                placeholder={'Добавить проект'}
              />
              {/* <label>Добавить пункт...</label> */}
            </div>
            <button
              className='admin-menu__button admin-menu__button_popup'
              onClick={props.addTodoButton}>
              <FaPlus className='admin-menu__button-img' />
            </button>

            {/* <ListBoard todos={props.state} /> */}
          </div>
        </div>
      </div>
    </div>
  );
}

export default Popup;
