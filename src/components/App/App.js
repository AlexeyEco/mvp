import React, { useEffect, useState, useReducer } from 'react';
import {
  Route,
  Switch,
  useHistory,
  Redirect,
  withRouter,
} from 'react-router-dom';
import * as auth from '../../utils/authorization';
import { CurrentUserContext } from '../../contexts/CurrentUserContext';
import { Context } from '../../contexts/context';
import Main from '../Main/Main';
import Map from '../Map/Map';
import Profile from '../Profile/Profile';
import Login from '../Login/Login';
import Register from '../Register/Register';
import ErrorPage from '../404/ErrorPage';
import MainApi from '../../utils/api/MainApi';
import InfoTooltip from '../InfoTooltip/InfoTooltip';
import PopupAddProject from '../PopupAddProject/PopupAddProject';
import ProtectedRoute from '../ProtectedRoute/ProtectedRoute';
import reducer from '../reducer/reducer';
import { FaAngellist, FaCheck, FaTimes } from 'react-icons/fa';
// import data from '../../data.json';

function App() {
  const [currentUser, setСurrentUser] = useState('');
  const history = useHistory();
  // const [savedCouriers, setSavedCouriers] = useState([]);
  const [loggedIn, setLoggedIn] = useState(false);
  const [infoTool, setInfoTool] = useState({
    message: '',
    img: '',
  });
  const [infoPopup, setInfoPopup] = useState();
  const [permissionsChecked, setPermissionsChecked] = useState(false);
  const [listBoard, setListBoard] = useState([]);
  const [state, dispatch] = useReducer(
    reducer,
    JSON.parse(localStorage.getItem('todos')) || []
  );
  const [todoTitle, setTodoTitle] = useState('');
  const [rating, setRating] = useState([]);

  const [data, setData] = useState({
    lanes: [
      {
        id: 'REITING',
        title: 'Рейтинг',
        label: `${rating.length}`,
        style: {
          justifyContent: 'space-between',
          width: 400,
          backgroundColor: '#DAD871',
        },
        cards: [
          // {
          //   id: 'Card1',
          //   name: 'John Smith',
          //   dueOn: 'due in a day',
          //   subTitle: 'SMS received at 12:13pm today',
          //   body: 'Thanks. Please schedule me for an estimate on Monday.',
          //   escalationText: 'Escalated to OPS-ESCALATIONS!',
          //   cardColor: '#BD3B36',
          //   cardStyle: {
          //     borderRadius: 6,
          //     boxShadow: '0 0 6px 1px #BD3B36',
          //     marginBottom: 15,
          //   },
          // },
          // {
          //   id: 'Card2',
          //   name: 'Card Weathers',
          //   dueOn: 'due now',
          //   subTitle: 'Email received at 1:14pm',
          //   body: 'Is the estimate free, and can someone call me soon?',
          //   escalationText: 'Escalated to Admin',
          //   cardColor: '#E08521',
          //   cardStyle: {
          //     borderRadius: 6,
          //     boxShadow: '0 0 6px 1px #E08521',
          //     marginBottom: 15,
          //   },
          // },
        ],
      },
      {
        disallowAddingCard: 'true',
        id: 'INPROCESS',
        title: 'В процессе',
        label: `${localStorage.getItem('length-cards')}`,
        style: {
          width: 400,
          backgroundColor: '#C1876B',
        },
        cards: [
          // {
          //   id: 'Card2',
          //   name: 'Alexey',
          //   dueOn: 'due in a day',
          //   subTitle: 'SMS received at 12:13pm today',
          //   body: 'Thanks. Please schedule me for an estimate on Monday.',
          //   escalationText: 'Escalated to OPS-ESCALATIONS!',
          //   cardColor: '#BD3B36',
          //   cardStyle: {
          //     borderRadius: 6,
          //     boxShadow: '0 0 6px 1px #BD3B36',
          //     marginBottom: 15,
          //   },
          // },
          // {
          //   id: 'Card2',
          //   name: 'Card Weathers',
          //   dueOn: 'due now',
          //   subTitle: 'Email received at 1:14pm',
          //   body: 'Is the estimate free, and can someone call me soon?',
          //   escalationText: 'Escalated to Admin',
          //   cardColor: '#E08521',
          //   cardStyle: {
          //     borderRadius: 6,
          //     boxShadow: '0 0 6px 1px #E08521',
          //     marginBottom: 15,
          //   },
          // },
        ],
      },
      {
        disallowAddingCard: 'true',
        id: 'COMPLETED',
        title: 'Завершенные',
        label: '2/5',
        style: {
          width: 400,
          backgroundColor: '#30BA8F',
        },
        cards: [
          // {
          //   id: 'Card3',
          //   name: 'Sergey',
          //   dueOn: 'due in a day',
          //   subTitle: 'SMS received at 12:13pm today',
          //   body: 'Thanks. Please schedule me for an estimate on Monday.',
          //   escalationText: 'Escalated to OPS-ESCALATIONS!',
          //   cardColor: '#BD3B36',
          //   cardStyle: {
          //     borderRadius: 6,
          //     boxShadow: '0 0 6px 1px #BD3B36',
          //     marginBottom: 15,
          //   },
          // },
          // {
          //   id: 'Card2',
          //   name: 'Card Weathers',
          //   dueOn: 'due now',
          //   subTitle: 'Email received at 1:14pm',
          //   body: 'Is the estimate free, and can someone call me soon?',
          //   escalationText: 'Escalated to Admin',
          //   cardColor: '#E08521',
          //   cardStyle: {
          //     borderRadius: 6,
          //     boxShadow: '0 0 6px 1px #E08521',
          //     marginBottom: 15,
          //   },
          // },
        ],
      },
    ],
    // lanes: [
    //   {
    //     // disallowAddingCard: 'true',
    //     id: 'REITING',
    //     title: 'Рейтинг',
    //     label: `${rating.length}`,
    //     style: {
    //       justifyContent: 'space-between',
    //       width: 280,
    //       backgroundColor: '#DAD871',
    //     },
    //     cards: [
    //       // {
    //       //   id: 'Milk',
    //       //   title: 'Buy milk',
    //       //   label: '15 mins',
    //       //   description: '2 Gallons of milk at the Deli store',
    //       // },
    //       // {
    //       //   id: 'Plan2',
    //       //   title: 'Dispose Garbage',
    //       //   label: '10 mins',
    //       //   description: 'Sort out recyclable and waste as needed',
    //       // },
    //       // {
    //       //   id: 'Plan3',
    //       //   title: 'Write Blog',
    //       //   label: '30 mins',
    //       //   description: 'Can AI make memes?',
    //       // },
    //       // {
    //       //   id: 'Plan4',
    //       //   title: 'Pay Rent',
    //       //   label: '5 mins',
    //       //   description: 'Transfer to bank account',
    //       // },
    //     ],
    //   },
    //   {
    //     disallowAddingCard: 'true',
    //     id: 'WIP',
    //     title: 'В процессе',
    //     label: `${localStorage.getItem('length-cards')}`,
    //     style: {
    //       width: 280,
    //       backgroundColor: '#C1876B',
    //     },
    //     cards: [
    //       // {
    //       //   id: 'Wip1',
    //       //   title: 'Clean House',
    //       //   label: '30 mins',
    //       //   description:
    //       //     'Soap wash and polish floor. Polish windows and doors. Scrap all broken glasses',
    //       // },
    //       // {
    //       //   id: 'Wip1',
    //       //   title: 'Clean House',
    //       //   label: '30 mins',
    //       //   description:
    //       //     'Soap wash and polish floor. Polish windows and doors. Scrap all broken glasses',
    //       // },
    //       // {
    //       //   id: 'Wip1',
    //       //   title: 'Clean House',
    //       //   label: '30 mins',
    //       //   description:
    //       //     'Soap wash and polish floor. Polish windows and doors. Scrap all broken glasses',
    //       // },
    //     ],
    //   },
    //   {
    //     disallowAddingCard: 'true',
    //     id: 'COMPLETED',
    //     title: 'Завершенные',
    //     label: '2/5',
    //     style: {
    //       width: 280,
    //       backgroundColor: '#30BA8F',
    //     },
    //     cards: [
    //       {
    //         id: 'Wip1',
    //         title: 'Clean House',
    //         label: '30 mins',
    //         description:
    //           'Soap wash and polish floor. Polish windows and doors. Scrap all broken glasses',
    //       },
    //     ],
    //   },
    // ],
  });

  // const [inProcess, setInProcess] = useState(lengthArrCards()[1]);

  function lengthArrCards(params) {
    return data.lanes.map((i) => i.cards.length);
  }

  // const arrData = data.lanes.map((i) => {
  //   let index;
  //   for (index = 0; index < i.cards.length; ++index) {
  //     return i.cards[index];
  //     // console.log(arrData[index]);
  //   }
  // });

  // const inProcess = lengthArrCards()[1];

  // console.log(arrData);

  useEffect(() => {
    localStorage.setItem('length-cards', JSON.stringify(lengthArrCards()[1]));
  }, [data]);

  useEffect(() => {
    localStorage.setItem('length-cards', JSON.stringify(lengthArrCards()[1]));
    localStorage.setItem('todos', JSON.stringify(state));
  }, [state]);

  const addTodo = (event) => {
    if (event.key === 'Enter') {
      dispatch({
        type: 'add',
        payload: todoTitle,
      });
      setTodoTitle('');
      closeAllPopups();
    }
  };

  function addTodoButton() {
    dispatch({
      type: 'add',
      payload: todoTitle,
    });
    setTodoTitle('');
    closeAllPopups();
  }

  useEffect(() => {
    const token = localStorage.getItem('jwt');
    if (token) {
      tokenCheck(token);
    }
    if (!token) {
      setPermissionsChecked(true);
    }
  }, []);

  function tokenCheck(token) {
    auth
      .getContent(token)
      .then((res) => {
        if (res) {
          setLoggedIn(true);
          history.push(location.pathname);
          setСurrentUser(res);
        }
      })
      .catch((res) => {
        console.log(`Ошибка: ${res}`);
        setPermissionsChecked(true);
      })
      .finally(() => {
        setPermissionsChecked(true);
      });
  }

  useEffect(() => {
    if (loggedIn) {
      MainApi.getUser()
        .then((res) => {
          setLoggedIn(true);
          setСurrentUser(res);
        })
        .catch((res) => {
          console.log(`Ошибка: ${res}`);
        });
    }
  }, [loggedIn]);

  function handleUpdateUser(data) {
    MainApi.setUserInfo(data)
      .then((res) => {
        setСurrentUser(res);
        setInfoPopup(true);
        setInfoTool({
          message: 'Данные изменены!',
          img: <FaCheck />,
        });
      })
      .catch((err) => console.log(err));
  }

  function handleRegister(data) {
    const { name, email, password } = data;
    auth
      .register(name, email, password)
      .then((res) => {
        if (res) {
          auth
            .authorize(email, password)
            .then((res) => {
              if (res.token) {
                localStorage.setItem('jwt', res.token);
                MainApi.setToken(res.token);
                return res.token;
              }
            })
            .then((token) => {
              auth
                .getContent(token)
                .then((res) => {
                  if (res) {
                    setLoggedIn(true);
                    setСurrentUser(res);
                    history.push('/map');
                  }
                })
                .catch((error) => {
                  console.log(`Ошибка: ${error}`);
                });
            })
            .catch((error) => {
              console.log(`Ошибка: ${error}`);
            });
          setInfoPopup(true);
          setInfoTool({
            message: 'Вы успешно зарегистрировались!',
            img: <FaAngellist />,
          });
        }
      })
      .catch((err) => {
        setInfoPopup(true);
        setInfoTool({
          message:
            'Что-то пошло не так! Такой пользователь уже зарегистрирован',
          img: <FaTimes />,
        });
        console.log(`Такой email существует в базе данных ${err}`);
      });
  }

  function handleLogin(data) {
    auth
      .authorize(data.email, data.password)
      .then((res) => {
        if (res.token) {
          localStorage.setItem('jwt', res.token);
          MainApi.setToken(res.token);
          return res.token;
        }
      })
      .then((token) => {
        auth
          .getContent(token)
          .then((res) => {
            if (res) {
              setLoggedIn(true);
              setСurrentUser(res);
              history.push('/map');
              setInfoPopup(true);
              setInfoTool({
                message: `Добро пожаловать ${res.name}!`,
                img: <FaAngellist />,
              });
            }
          })
          .catch((error) => {
            console.log(`Ошибка: ${error}`);
          });
      })
      .catch((error) => {
        setInfoPopup(true);
        setInfoTool({
          message: 'Что-то пошло не так! Введите правильно данные',
          img: <FaTimes />,
        });
        console.log(`Ошибка: ${error}`);
      });
  }

  function signOut() {
    setСurrentUser('');
    setLoggedIn(false);
    history.push('/');
  }

  function closeAllPopups() {
    setInfoPopup(false);
  }

  const escFunction = (event) => {
    if (event.keyCode === 27) {
      closeAllPopups();
    }
  };

  useEffect(() => {
    document.addEventListener('keydown', escFunction, false);
    return () => {
      document.removeEventListener('keydown', escFunction, false);
    };
  });

  if (!permissionsChecked) {
    return null;
  }

  return (
    <CurrentUserContext.Provider value={currentUser}>
      <Context.Provider
        value={{
          dispatch,
        }}>
        <div className='app'>
          <Switch>
            <Route exact path='/'>
              <Main loggedIn={loggedIn} />
            </Route>
            <ProtectedRoute
              path='/map'
              component={Map}
              permissionsChecked={permissionsChecked}
              loggedIn={loggedIn}
              popupAddProject={setInfoPopup}
              data={data}
              // arrData={arrData}
              state={state}
              listBoard={listBoard}
              // savedCouriers={savedCouriers}
            />
            {/* <ProtectedRoute
            path='/saved-couriers'
            permissionsChecked={permissionsChecked}
            loggedIn={loggedIn}
            savedCouriers={savedCouriers}
          /> */}
            <ProtectedRoute
              path='/profile'
              component={Profile}
              permissionsChecked={permissionsChecked}
              loggedIn={loggedIn}
              signOut={signOut}
              handleUpdateUser={handleUpdateUser}
            />
            {/* <ProtectedRoute
            path='/add-couriers'
            permissionsChecked={permissionsChecked}
            loggedIn={loggedIn}
            signOut={signOut}
          /> */}
            <Route path='/signin'>
              <Login handleLogin={handleLogin} />
            </Route>
            <Route path='/signup'>
              <Register handleRegister={handleRegister} />
            </Route>
            <Route path='*' exact component={ErrorPage} />
            <Route>
              <Redirect from='*' to='/errors' />
            </Route>
          </Switch>
          <InfoTooltip
            isOpen={infoPopup}
            onClose={closeAllPopups}
            infoTool={infoTool}
          />
          <PopupAddProject
            isOpen={infoPopup}
            onClose={closeAllPopups}
            infoTool={infoTool}
            todoTitle={todoTitle}
            addTodo={addTodo}
            addTodoButton={addTodoButton}
            // state={state}
            setTodoTitle={setTodoTitle}
          />
        </div>
      </Context.Provider>
    </CurrentUserContext.Provider>
  );
}

export default withRouter(App);
