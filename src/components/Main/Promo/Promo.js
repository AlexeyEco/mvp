import React from 'react';
import { FaListAlt } from 'react-icons/fa';

function Promo() {
  return (
    <div className='promo'>
      <FaListAlt className='promo__logo' />
      <h1 className='promo__title'>Для входа в личный кабинет пройдите регистрацию или авторизуйтесь</h1>
    </div>
  );
}

export default Promo;
