import React from 'react';
import Header from '../Header/Header';
import Promo from './Promo/Promo';

function Main(props) {
  return (
    <main className='main'>
      <Header loggedIn={props.loggedIn} />
      <Promo />
    </main>
  );
}

export default Main;
