import React from 'react';
import Navigation from '../Navigation/Navigation';

function Header(props) {
  return (
    <header className='header'>
      <div className='header__nav'>

        <Navigation loggedIn={props.loggedIn} />
      </div>
    </header>
  );
}

export default Header;
