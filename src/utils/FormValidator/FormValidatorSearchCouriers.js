import * as yup from 'yup';

const validationSchema = yup.object().shape({
  search: yup.string('').required('Нужно ввести Фамилию'),
});

export default validationSchema;
