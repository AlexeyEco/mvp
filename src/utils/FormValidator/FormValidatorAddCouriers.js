import * as yup from 'yup';

const phoneRegExp = /^((\\+[1-9]{1,4}[ \\-]*)|(\\([0-9]{2,3}\\)[ \\-]*)|([0-9]{2,4})[ \\-]*)*?[0-9]{3,4}?[ \\-]*[0-9]{3,4}?$/;

const validationSchema = yup.object().shape({
  name: yup
    .string()
    .min(2, 'Минимум 2 символа')
    .max(30, 'Максимум 30 символов')
    .typeError('Должно быть строкой')
    .required('Обязательное поле'),
  surname: yup
    .string()
    .min(2, 'Минимум 2 символа')
    .max(30, 'Максимум 30 символов')
    .typeError('Должно быть строкой')
    .required('Обязательное поле'),
  foto: yup
    .string()
    .url('Должна быть ссылка на фото')
    .typeError('Должно быть строкой')
    .required('Обязательное поле'),
  email: yup
    .string()
    .email('Введите правильно Email')
    .required('Обязательное поле'),
  telefonNumber: yup
    .string()
    .matches(phoneRegExp, 'Невалидный номер телефона')
    .max(11, 'Максимум 11 символов')
    .required('Обязательное поле'),
  deviceModel: yup
    .string()
    .min(2, 'Минимум 2 символа')
    .max(30, 'Максимум 30 символов')
    .typeError('Должно быть строкой')
    .required('Обязательное поле'),
  deviceId: yup
    .number()
    .typeError('Должны быть цифры')
    .required('Обязательное поле'),
});

export default validationSchema;
