const thenCouriersApi = (res) => {
  if (res.ok) {
    return res.json();
  }
  return Promise.reject(`Ошибка: ${res.status}`);
};

class CouriersApi {
  constructor(options) {
    this._baseUrl = options.baseUrl;
    this._headers = options.headers;
  }

  getInitialCouriers() {
    return fetch(`${this._baseUrl}/saved-couriers`, {
      headers: this._headers,
    }).then(thenCouriersApi);
  }

  addCourier(data) {
    return fetch(`${this._baseUrl}/add-couriers`, {
      method: 'POST',
      headers: this._headers,
      body: JSON.stringify({
        name: data.name,
        surname: data.surname,
        foto: data.foto,
        email: data.email,
        telefonNumber: data.telefonNumber,
        deviceModel: data.deviceModel,
        deviceId: data.deviceId,
        deviceData: 'Информация с телефона',
        latitude: data.latitude,
        longitude: data.longitude,
      }),
    });
  }

  cardDelete(card) {
    return fetch(`${this._baseUrl}/saved-couriers/${card._id}`, {
      method: 'DELETE',
      headers: this._headers,
    }).then(thenCouriersApi);
  }
}

const api = new CouriersApi({
  baseUrl: 'https://api.courier-search-service.ru',
  headers: {
    Authorization: `Bearer ${localStorage.getItem('jwt')}`,
    'Content-Type': 'application/json',
  },
});

export default api;
